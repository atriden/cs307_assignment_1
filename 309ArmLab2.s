@ Filename: ArmLab2AlecRiden.s
@ Author:   Alec Riden
@ Email:    ar0119@uah.edu
@ CS309-01 Spring 2019

.global main @ Have to use main because of C library uses.

main:

@*******************

Start:
@*******************
	@r4 Coke stock
	@r5 Sprite stock
	@r6 Dr. Pep stock
	@r7 Diet Coke stock
	@r8 Mellow stock
	@r9 Total stock
	
	
	
	ldr r0, =strWelcomePrompt @ Put the address of my string into the first parameter
	bl  printf              @ Call the C printf to display input prompt.

	mov r4, #5
	mov r5, #5
	mov r6, #5
	mov r7, #5
	mov r8, #5
	mov r9, #25

	ldr r0, =intCoke
	str r4, [r0]

	ldr r0, =intSprite
	str r5, [r0]
	
	ldr r0, =intPepper
	str r6, [r0]
	
	ldr r0, =intDiet
	str r7, [r0]
	
	ldr r0, =intMellow
	str r8, [r0]
	
	ldr r0, =intStock
	str r9, [r0]
@*******************

VendStart:
@*******************

	@r4- drinkStock
	@r5- totalStock
	@r6- 
	@r7- change
	@r8- input
	@r9- Money given
	
	
	ldr r0, =intStock
	ldr r5, [r0]
	
	cmp r5, #0
	beq closeUp
	
	mov r9, #0
	
@*******************

	MoneyLoop:
@*******************

	cmp r9, #55
	bge select
	
	
	ldr r0, =strMoneyPrompt
	bl printf

	ldr r0, =strCharInPattern
	ldr r1, =charChoice
	bl scanf

	ldr r0, =charChoice
	ldr r8, [r0]

	cmp r8, #'B'
	beq bill
	cmp r8, #'b'
	beq bill
	
	cmp r8, #'D'
	beq dime
	cmp r8, #'d'
	beq dime
	
	cmp r8, #'N'
	beq nickel
	cmp r8, #'n'
	beq nickel
	
	cmp r8, #'Q'
	beq quarter
	cmp r8, #'q'
	beq quarter
	
	cmp r8, #'S'
	beq checkStock
	cmp r8, #'s'
	beq checkStock
	
	
	b invalid
	
@*******************

bill:
@*******************
	
	add r9, r9, #100
	
	ldr r0, =strTotalPrompt
	mov r1, r9
	bl printf
	
	b MoneyLoop
	
@*******************

dime:
@*******************
	
	add r9, r9, #10
	
	ldr r0, =strTotalPrompt
	mov r1, r9
	bl printf
	
	b MoneyLoop
	
@*******************

quarter:
@*******************

	add r9, r9, #25
	
	ldr r0, =strTotalPrompt
	mov r1, r9
	bl printf
	
	b MoneyLoop
	
@*******************

nickel:
@*******************
	
	add r9, r9, #5
	
	ldr r0, =strTotalPrompt
	mov r1, r9
	bl printf
	
	b MoneyLoop

@*******************

invalid:
@*******************

	ldr r0, =strInvalid
	bl printf
	
	ldr r0, =strTotalPrompt
	mov r1, r9
	bl printf
	
	b MoneyLoop
	
@*******************

select:
@*******************
	@r4- 
	@r5- 
	@r6- 
	@r7- change
	@r8- input
	@r9- Money given
	

	ldr r0, =strSelectPrompt
	bl printf
	
	ldr r0, =strCharInPattern
	ldr r1, =charChoice
	bl scanf
	
	ldr r0, =charChoice
	ldr r8, [r0]
	
	cmp r8, #'C'
	beq coke
	cmp r8, #'c'
	beq coke
	
	cmp r8, #'S'
	beq sprite
	cmp r8, #'s'
	beq sprite
	
	cmp r8, #'P'
	beq pepper
	cmp r8, #'p'
	beq pepper
	
	cmp r8, #'D'
	beq diet
	cmp r8, #'d'
	beq diet
	
	cmp r8, #'M'
	beq mellow
	cmp r8, #'m'
	beq mellow
	
	cmp r8, #'X'
	beq cancel
	cmp r8, #'x'
	beq cancel
	
	b invalid

@*******************

coke:
@*******************
	
	ldr r0, =intCoke
	ldr r4, [r0]
	
	ldr r0, =strCokePrompt
	bl printf
	
	cmp r4, #0
	beq outOstock
	
	ldr r0, =strCokeConfirm
	bl printf
	
	ldr r0, =strCharInPattern
	ldr r1, =charChoice
	bl scanf
	
	ldr r0, =charChoice
	ldr r8, [r0]
	
	cmp r8, #'N'
	beq select
	cmp r8, #'n'
	beq select
	
	ldr r0, =intStock
	ldr r5, [r0]
	
	sub r7, r9, #55
	sub r4, r4, #1
	sub r5, r5, #1
	
	ldr r0, =intCoke
	str r4, [r0]
	ldr r0, =intStock
	str r5, [r0]
	
	ldr r0, =strCokeChange
	mov r1, r7
	bl printf
	
	b VendStart
	
@*******************

sprite:
@*******************

	ldr r0, =intSprite
	ldr r4, [r0]
	
	ldr r0, =strSpritePrompt
	bl printf
	
	cmp r4, #0
	beq outOstock
	
	ldr r0, =strSpriteConfirm
	bl printf
	
	ldr r0, =strCharInPattern
	ldr r1, =charChoice
	bl scanf
	
	ldr r0, =charChoice
	ldr r8, [r0]
	
	cmp r8, #'N'
	beq select
	cmp r8, #'n'
	beq select
	
	ldr r0, =intStock
	ldr r5, [r0]
	
	sub r7, r9, #55
	sub r4, r4, #1
	sub r5, r5, #1
	
	ldr r0, =intSprite
	str r4, [r0]
	ldr r0, =intStock
	str r5, [r0]
	
	ldr r0, =strSpriteChange
	mov r1, r7
	bl printf
	
	b VendStart

@*******************

pepper:
@*******************

	ldr r0, =intPepper
	ldr r4, [r0]

	ldr r0, =strDrPrompt
	bl printf
	
	cmp r4, #0
	beq outOstock
	
	ldr r0, =strDrConfirm
	bl printf
	
	ldr r0, =strCharInPattern
	ldr r1, =charChoice
	bl scanf
	
	ldr r0, =charChoice
	ldr r8, [r0]
	
	cmp r8, #'N'
	beq select
	cmp r8, #'n'
	beq select
	
	ldr r0, =intStock
	ldr r5, [r0]
	
	sub r7, r9, #55
	sub r4, r4, #1
	sub r5, r5, #1
	
	ldr r0, =intPepper
	str r4, [r0]
	ldr r0, =intStock
	str r5, [r0]
	
	ldr r0, =strDrChange
	mov r1, r7
	bl printf
	
	b VendStart

@*******************

diet:
@*******************

	ldr r0, =intDiet
	ldr r4, [r0]
	
	ldr r0, =strDietCokePrompt
	bl printf
	
	cmp r4, #0
	beq outOstock
	
	ldr r0, =strDietConfirm
	bl printf
	
	ldr r0, =strCharInPattern
	ldr r1, =charChoice
	bl scanf
	
	ldr r0, =charChoice
	ldr r8, [r0]
	
	cmp r8, #'N'
	beq select
	cmp r8, #'n'
	beq select
	
	ldr r0, =intStock
	ldr r5, [r0]
	
	sub r7, r9, #55
	sub r4, r4, #1
	sub r5, r5, #1
	
	ldr r0, =intDiet
	str r4, [r0]
	ldr r0, =intStock
	str r5, [r0]
	
	ldr r0, =strDietChange
	mov r1, r7
	bl printf
	
	b VendStart

@*******************

mellow:
@*******************
	
	ldr r0, =intMellow
	ldr r4, [r0]
	
	ldr r0, =strMellowPrompt
	bl printf
	
	cmp r4, #0
	beq outOstock
	
	ldr r0, =strMellowConfirm
	bl printf
	
	ldr r0, =strCharInPattern
	ldr r1, =charChoice
	bl scanf
	
	ldr r0, =charChoice
	ldr r8, [r0]
	
	cmp r8, #'N'
	beq select
	cmp r8, #'n'
	beq select
	
	ldr r0, =intStock
	ldr r5, [r0]
	
	sub r7, r9, #55
	sub r4, r4, #1
	sub r5, r5, #1
	
	ldr r0, =intMellow
	str r4, [r0]
	ldr r0, =intStock
	str r5, [r0]
	
	ldr r0, =strMellowChange
	mov r1, r7
	bl printf
	
	b VendStart

@*******************

cancel:
@*******************

	mov r7, r9
	
	ldr r0, =strCancelChange
	mov r1, r7
	bl printf
	
	b VendStart
	
@*******************
checkStock:
@*******************

	ldr r0, =intCoke
	ldr r4, [r0]

	ldr r0, =intSprite
	ldr r5, [r0]
	
	ldr r0, =intPepper
	ldr r6, [r0]
	
	ldr r0, =intDiet
	ldr r7, [r0]
	
	ldr r0, =intMellow
	ldr r8, [r0]
	
	ldr r0, =strCoke
	mov r1, r4
	bl printf
	
	ldr r0, =strSprite
	mov r1, r5
	bl printf
	
	ldr r0, =strDr
	mov r1, r6
	bl printf
	
	ldr r0, =strDiet
	mov r1, r7
	bl printf
	
	ldr r0, =strMellow
	mov r1, r8
	bl printf
	
	b VendStart
	
@*******************

outOstock:
@*******************
	
	ldr r0, =strOutOfStockPrompt
	bl printf
	b select

@*******************

closeUp:
@*******************

	ldr r0, =strClosingPrompt
	bl printf

@*******************

my_exit:
@*******************
@ End of my code. Force the exit and return control to OS

	mov r7, #0x01 @SVC call to exit
	svc 0         @Make the system call.

@**********************************************************************

.data
@ Declare the strings and data needed

	.balign 4
	strWelcomePrompt: .asciz "Welcome to Mr. Zippy’s soft drink vending machine. \n Cost of Coke, Sprite, Dr. Pepper, Diet Coke, and Mellow Yellow is 55 cents.\n"

	.balign 4
	strMoneyPrompt: .asciz "Enter money nickel (N), dime (D), quarter (Q), and one dollar bill (B). \n"

	.balign 4
	strTotalPrompt: .asciz "Total is %d cents.\n"

	.balign 4
	strSelectPrompt: .asciz "Make selection: \n (C) Coke, (S) Sprite, (P) Dr. Pepper, (D) Diet Coke, or (M) Mellow Yellow (X) to cancel and return all money. \n"
	
	.balign 4
	strCokePrompt: .asciz "Selection is Coke.\n"
	
	.balign 4
	strSpritePrompt: .asciz "Selection is Sprite.\n"
	
	.balign 4
	strDrPrompt: .asciz "Selection is Dr. Pepper.\n"
	
	.balign 4
	strDietCokePrompt: .asciz "Selection is Diet Coke.\n"
	
	.balign 4
	strMellowPrompt: .asciz "Selection is Mellow Yellow.\n"
	
	.balign 4
	strCokeChange: .asciz "Coke has been dispensed with %d cents change\n"
	
	.balign 4
	strSpriteChange: .asciz "Sprite has been dispensed with %d cents change\n"
	
	.balign 4
	strDrChange: .asciz "Dr. Pepper has been dispensed with %d cents change\n"
	
	.balign 4
	strDietChange: .asciz "Diet Coke has been dispensed with %d cents change\n"
	
	.balign 4
	strMellowChange: .asciz "Mellow Yellow has been dispensed with %d cents change\n"
	
	.balign 4
	strCancelChange: .asciz "No drink has been dispensed returning %d cents...\n"
	
	.balign 4
	strCokeConfirm: .asciz "Are your sure you want a Coke? (Y or N)\n"
	
	.balign 4
	strSpriteConfirm: .asciz "Are your sure you want a Sprite? (Y or N)\n"
	
	.balign 4
	strDrConfirm: .asciz "Are your sure you want a Dr. Pepper? (Y or N)\n"
	
	.balign 4
	strDietConfirm: .asciz "Are your sure you want a Diet Coke? (Y or N)\n"
	
	.balign 4
	strMellowConfirm: .asciz "Are your sure you want a Mellow Yellow? (Y or N)\n"
	
	.balign 4
	strCharInPattern: .asciz "%s"
	
	.balign 4
	strCoke: .asciz "Coke: %d\n"
	
	.balign 4
	strSprite: .asciz "Sprite: %d\n"
	
	.balign 4
	strDr: .asciz "Dr. Pepper: %d\n"
	
	.balign 4
	strDiet: .asciz "Diet Coke: %d\n"
	
	.balign 4
	strMellow: .asciz "Mellow Yellow: %d\n"
	
	.balign 4
	strOutOfStockPrompt: .asciz "We're sorry, that drink is out of stock.\n"
	
	.balign 4
	strClosingPrompt: .asciz "We're sorry, this machine is currently empty. Goodbye.\n"
	
	.balign 4
	strInvalid: .asciz "Input invalid\n"

	.balign 4 
	charChoice: .word 4

	.balign 4 
	intCurrentPay: .word 0

	.balign 4 
	intChange: .word 0
	
	.balign 4 
	intCoke: .word 0
	
	.balign 4 
	intSprite: .word 0
	
	.balign 4 
	intPepper: .word 0
	
	.balign 4 
	intDiet: .word 0
	
	.balign 4 
	intMellow: .word 0
	
	.balign 4 
	intStock: .word 0


@ Let the assembler know these are the C library functions.

	.global printf
	.global scanf
